<?php
/**
 * Copyright ©  kowal sp. z o.o. All rights reserved.
 * See COPYING.txt for license details.
 */
declare(strict_types=1);

namespace Kowal\DisableAddToCart\Plugin\Frontend\Magento\Catalog\Model;

class Product
{
    public function __construct(
        \Kowal\DisableAddToCart\Helper\Data $helperData
    )
    {
        $this->helperData = $helperData;

    }

    public function afterIsSaleable(
        \Magento\Catalog\Model\Product $subject,
        $result
    )
    {
        $this->enable = $this->helperData->getGeneralCfg("enable", $subject->getStoreId());
        if ($this->enable == 1) {
            return [];
        } else {
            return $result;
        }

    }
}

